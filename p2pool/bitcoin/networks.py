import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc
from operator import *


@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    onecoin=math.Object(
        P2P_PREFIX='c8c9cacb'.decode('hex'),
        P2P_PORT=1998,
        ADDRESS_VERSION=115,
        RPC_PORT=1997,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            '1coinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),                           
        SUBSIDY_FUNC=lambda height: int(15.15*100000000) >> (height + 1)//680000,
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('onecoin_3s').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('onecoin_3s').getPoWHash(data)),
        BLOCK_PERIOD=60, # s
        SYMBOL='ONE',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], '1Coin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/1Coin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.1coin'), '1coin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://explorer.1coin.io/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://explorer.1coin.io/address/',
        TX_EXPLORER_URL_PREFIX='http://explorer.1coin.io/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1), 
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
        CHARITY_ADDRESS='a4f8d278379021de2bb6a78979a184957344bed7'.decode('hex'),
    ),



)
for net_name, net in nets.iteritems():
    net.NAME = net_name
